import { createSlice } from '@reduxjs/toolkit';
import { getItemIndex } from 'utils/helpers';
import toast from 'react-hot-toast';

const initialState = {
  cartList: [],
  totalPrice: 0,
  favoriteList: [],
  isSuccess: false,
  isError: false,
  isLoading: false
};

const calculateTotal = (arr = []) => arr.reduce((a, b) => a + b.price * b.quantity, 0);

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addToCart(state, { payload }) {
      const itemIndex = getItemIndex(state.cartList, payload.name);
      if (itemIndex < 0) {
        state.cartList.push(payload);
        state.totalPrice = calculateTotal(state.cartList);
        toast.success('Успешно добавлен в корзину');
      } else {
        state.cartList[itemIndex].quantity += payload.quantity;
        state.totalPrice = calculateTotal(state.cartList);
        toast('Уже в корзине');
      }
    },
    removeFromCart(state, action) {
      return { ...state, cartList: state.cartList.filter((item) => item.name !== action.payload) };
    },
    addToFavorite(state, { payload }) {
      const itemIndex = getItemIndex(state.favoriteList, payload.name);
      if (itemIndex < 0) {
        state.favoriteList.push(payload);
        toast.success('Успешно добавлено в избранное');
      } else {
        toast('Уже в избранном');
      }
    },
    removeFromFavorite(state, action) {
      // return state.cartList.filter((item) => item.id !== action.payload.id);
      return {
        ...state,
        favoriteList: state.favoriteList.filter((item) => item.name !== action.payload)
      };
    },
    incrementQuantity(state, { payload }) {
      const itemIndex = getItemIndex(state.cartList, payload.name);
      state.cartList[itemIndex].quantity += 1;
      state.totalPrice = calculateTotal(state.cartList);
    },
    decrementQuantity(state, { payload }) {
      const itemIndex = getItemIndex(state.cartList, payload.name);
      if (state.cartList[itemIndex].quantity > 1) {
        state.cartList[itemIndex].quantity -= 1;
        state.totalPrice = calculateTotal(state.cartList);
      } else {
        state.cartList.splice(itemIndex, 1);
        state.totalPrice = calculateTotal(state.cartList);
      }
    },
    batchRemove(state, action) {
      return state.cartList.filter((item) => !action.payload.ids.includes(item.id));
    },
    clearCart: (state) => {
      state.cartList = [];
      state.totalPrice = 0;
    },
    clearFavorite: (state) => {
      state.favoriteList = [];
    },
    clearAll: () => initialState
  },
  extraReducers: {}
});

export const {
  addToCart,
  removeFromCart,
  addToFavorite,
  removeFromFavorite,
  incrementQuantity,
  decrementQuantity,
  batchRemove,
  clearAll,
  clearCart,
  clearFavorite,
  toggleInclude
} = cartSlice.actions;
