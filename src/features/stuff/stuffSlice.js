import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  theme: 'light',
  currentLanguage: 'ru',
  redirectUrl: '/dashboard'
};

export const stuffSlice = createSlice({
  name: 'stuff',
  initialState,
  reducers: {
    changeTheme(state, action) {
      state.theme = action.payload;
    },
    setUrl(state, action) {
      state.redirectUrl = action.payload;
    }
  },
  extraReducers: {}
});

export const { changeTheme, setUrl } = stuffSlice.actions;
