import L from 'leaflet';

export const MapMarker = L.Icon.extend({
  options: {
    // shadowUrl: 'leaf-shadow.png',
    iconSize: 36,
    popupAnchor: [0, -18]
  }
});

export const getLatLngBounds = (arr) => arr.map((a) => [a.latitude, a.longitude]);

export const createCLusterIcon = (cluster) => {
  const count = cluster.getChildCount();

  return L.divIcon({
    html: `<div>
        <span class="markerClusterLabel">${count}</span>
      </div>`,
    className: 'customMarkerCluster'
  });
};
