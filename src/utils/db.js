import faker from 'faker';
import { sample } from 'lodash';
// utils
import { mockImgProduct, mockImgProductSaunet } from 'utils/mockImages';

// ----------------------------------------------------------------------

export const SHOESNAME = [
  'Nike Air Force 1 NDESTRUKT',
  'Nike Space Hippie 04',
  'Nike Air Zoom Pegasus 37 A.I.R. Chaz Bear',
  'Nike Blazer Low 77 Vintage',
  'Nike ZoomX SuperRep Surge',
  'Zoom Freak 2',
  'Nike Air Max Zephyr',
  'Jordan Delta',
  'Air Jordan XXXV PF',
  'Nike Waffle Racer Crater',
  'Kyrie 7 EP Sisterhood',
  'Nike Air Zoom BB NXT',
  'Nike Air Force 1 07 LX',
  'Nike Air Force 1 Shadow SE',
  'Nike Air Zoom Tempo NEXT%',
  'Nike DBreak-Type',
  'Nike Air Max Up',
  'Nike Air Max 270 React ENG',
  'NikeCourt Royale',
  'Nike Air Zoom Pegasus 37 Premium',
  'Nike Air Zoom SuperRep',
  'NikeCourt Royale',
  'Nike React Art3mis',
  'Nike React Infinity Run Flyknit A.I.R. Chaz Bear'
];

const BUDSNAME = [
  'GOAT MILK (козье молоко)',
  'LIBEDOR',
  'LIBIDUM-M',
  'NEOVINOLS',
  'PEP-TANF BEAUTY',
  'PEP-TANF HEALTH',
  'PROSTATINOR',
  'SAUMAL',
  'SHUBAT',
  'МАСЛО ЧЕРНОГО ТМИНА'
];

const FAKESNAME = [
  'Pigeon By stovekraft Amaze Plus 1.5 Litre Electric kettle',
  'Airpods Wireless Bluetooth Headphones',
  'Panasonic 584 litres Side-by-Side Refrigerator',
  'Sony Playstation 4 Pro White Version',
  'Amazon Echo Dot 3rd Generation',
  'OnePlus 138.8 cm Q1 Series 4K Android QLED TV',
  'Fossil Stevie Crossbody',
  'Versace EROS Samples/Decants',
  'Round Shiny Black 100% UV Protected Sunglasses',
  'Lace-Up Stiletto Heels',
  'Cannon EOS 80D DSLR Camera',
  'iPhone 11 Pro 256GB Memory',
  'Gabrielle Chanel EDP Samples/Decants',
  'Samsung 23 L Solo Microwave Oven',
  ' Nike Mens Viale Running Shoes ',
  'Fastrack Black Square Sunglasses for Guys',
  'FOSSIL Charter Hybrid HR FTW7013 Smart Watch',
  'Samsung 9 Kg Fully Automatic Washing Machine',
  'Fossil Sport Backpack',
  'Nate Chronograph Black Stainless Steel Watch',
  'Logitech G-Series Gaming Mouse',
  'Pen drive',
  'Speaker',
  'Sony PlayStation 5 PS5 Console Complete Set'
];

// ----------------------------------------------------------------------

const sportShoes = SHOESNAME.map((name, index) => {
  const setIndex = index + 1;
  return {
    id: faker.datatype.uuid(),
    cover: mockImgProduct(setIndex),
    smallPhotos: [faker.image.image(), faker.image.image(), faker.image.image()],
    name,
    code: faker.datatype.number(),
    price: faker.datatype.number({ min: 8000, max: 70000 }),
    priceSale:
      setIndex % 2 ? null : faker.datatype.number({ min: 8000, max: 70000, precision: 0.01 }),
    status: sample(['Скидка', 'Новинка', '', '']),
    description: faker.commerce.productDescription(),
    lorem: faker.lorem.paragraphs(3),
    rating: faker.datatype.number({ min: 0, max: 5 }),
    comment: faker.datatype.number({ min: 0, max: 200 })
  };
});

const buds = BUDSNAME.map((name, index) => {
  const setIndex = index + 1;
  return {
    id: faker.datatype.uuid(),
    cover: mockImgProduct(setIndex),
    smallPhotos: [faker.image.image(), faker.image.image(), faker.image.image()],
    name,
    code: faker.datatype.number(),
    price: faker.datatype.number({ min: 8000, max: 70000 }),
    priceSale:
      setIndex % 2 ? null : faker.datatype.number({ min: 8000, max: 70000, precision: 0.01 }),
    status: sample(['Скидка', 'Новинка', '', '']),
    description: faker.commerce.productDescription(),
    lorem: faker.lorem.paragraphs(3),
    rating: faker.datatype.number({ min: 0, max: 5 }),
    comment: faker.datatype.number({ min: 0, max: 200 })
  };
});

const fake = FAKESNAME.map((name, index) => {
  const setIndex = index + 1;
  return {
    id: faker.datatype.uuid(),
    cover: faker.image.image(),
    smallPhotos: [faker.image.image(), faker.image.image(), faker.image.image()],
    name,
    code: faker.datatype.number(),
    price: faker.datatype.number({ min: 8000, max: 70000 }),
    priceSale:
      setIndex % 2 ? null : faker.datatype.number({ min: 8000, max: 70000, precision: 0.01 }),
    status: sample(['Скидка', 'Новинка', '', '']),
    description: faker.commerce.productDescription(),
    lorem: faker.lorem.paragraphs(3),
    rating: faker.datatype.number({ min: 0, max: 5 }),
    comment: faker.datatype.number({ min: 0, max: 200 })
  };
});

export default {
  categories: [
    {
      code: 1,
      name: 'Обувь',
      subCategories: [
        {
          code: 2,
          name: 'Кроссовки',
          products: sportShoes
        }
      ]
    },
    {
      code: 3,
      name: 'Медицина',
      subCategories: [
        {
          code: 4,
          name: 'Бады',
          products: buds
        }
      ]
    },
    {
      code: 5,
      name: 'Случайное',
      subCategories: [
        {
          code: 6,
          name: faker.commerce.department(),
          products: fake
        }
      ]
    }
  ]
};

// {
//   code: 81,
//   name: '',
//   description: '',
//   photoUrl: '',
//   shopName: '',
//   price: 0,
//   sizes: []
// },
