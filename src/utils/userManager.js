import * as ReduxOidc from 'redux-oidc';
import * as Oidc from 'oidc-client';
import { CookieStorage } from 'cookie-storage';

const env = !process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? 'dev' : 'prod';

const cookieStorage = new CookieStorage({
  expires: new Date(Date.now() + 2000000)
});

const userManagerConfig =
  env === 'prod'
    ? {
        client_id: 'BzarFrontClient',
        redirect_uri: 'https://bzar.kz/login_complete',
        response_type: 'id_token token',
        post_logout_redirect_uri: 'https://bzar.kz/logout',
        scope: 'openid profile bpmnapi frontapi account_information roles email',
        authority: 'https://fsauth.bzar.kz',
        silent_redirect_uri: 'https://bzar.kz/login_complete',
        loadUserInfo: true,
        checkSessionInterval: 60,
        automaticSilentRenew: true,
        response_mode: 'fragment',
        userStore: new Oidc.WebStorageStateStore({ store: cookieStorage })
      }
    : {
        client_id: 'BzarFrontClientMadiyarLocalDevelopment',
        redirect_uri: 'http://localhost:3000/login_complete',
        response_type: 'id_token token',
        post_logout_redirect_uri: 'http://localhost:3000/logout',
        scope: 'openid profile bpmnapi frontapi account_information roles email',
        authority: 'https://localhost:5002',
        silent_redirect_uri: 'http://localhost:3000/login_complete"',
        loadUserInfo: true,
        checkSessionInterval: 60,
        automaticSilentRenew: true,
        response_mode: 'fragment',
        userStore: new Oidc.WebStorageStateStore({ store: cookieStorage })
      };

const userManager = ReduxOidc.createUserManager(userManagerConfig);

export default userManager;
