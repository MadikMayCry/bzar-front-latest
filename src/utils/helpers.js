export const getItemIndex = (arr = [], nameToFind) => {
  const names = arr.map((item) => item.name);
  return names.indexOf(nameToFind);
};

export function isEmptyObject(obj) {
  if (obj) {
    return Object.keys(obj).length === 0;
  }
  return true;
}

export const isDarkTheme = (mode) => mode === 'dark';

export function findById(o, name) {
  // Early return
  if (o === null) return;
  if (o.name === name) return o;

  let result;
  Object.keys(o).every((p) => {
    if (Object.prototype.hasOwnProperty.call(o, p) && typeof o[p] === 'object') {
      result = findById(o[p], name);

      if (result) {
        return false;
      }
    }
    return true;
  });
  return result;
}
