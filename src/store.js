import { configureStore, getDefaultMiddleware, combineReducers } from '@reduxjs/toolkit';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist';
import { reducer as oidcReducer } from 'redux-oidc';
import storage from 'redux-persist/lib/storage';
import { userSlice } from 'features/user/userSlice';
import { stuffSlice } from 'features/stuff/stuffSlice';
import { cartSlice } from 'features/cart/cartSlice';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

const reducers = combineReducers({
  user: userSlice.reducer,
  stuff: stuffSlice.reducer,
  cart: cartSlice.reducer,
  oidc: oidcReducer
  // cabinetMenu: cabinetMenuSlice.reducer,
});

const persistConfig = {
  key: 'root',
  storage,
  stateReconciler: autoMergeLevel2
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
    }
  })
});

export const persistor = persistStore(store);
