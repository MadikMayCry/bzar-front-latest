import { merge } from 'lodash';
import ReactApexChart from 'react-apexcharts';
// material
import { Box, Card, CardHeader } from '@material-ui/core';
// utils
import { fNumber } from '../../../utils/formatNumber';
//
import { BaseOptionChart } from '../../charts';
import { PRODUCT_NAME } from '../../../_mocks_/products';

// ----------------------------------------------------------------------

const CHART_DATA = [
  {
    data: [1400, 1390, 1233, 1111, 540, 580, 1090, 1100, 1200, 1380].sort((a, b) =>
      a > b ? -1 : 1
    )
  }
];

export default function AppConversionRates() {
  const chartOptions = merge(BaseOptionChart(), {
    tooltip: {
      marker: { show: false },
      y: {
        formatter: (seriesName) => fNumber(seriesName),
        title: {
          formatter: (seriesName) => `#${seriesName}`
        }
      }
    },
    plotOptions: {
      bar: { horizontal: true, barHeight: '28%', borderRadius: 2 }
    },
    xaxis: {
      categories: PRODUCT_NAME.slice(0, 10)
    }
  });

  return (
    <Card>
      <CardHeader title="Самые продаваемые товары" />
      <Box sx={{ mx: 3 }} dir="ltr">
        <ReactApexChart type="bar" series={CHART_DATA} options={chartOptions} height={364} />
      </Box>
    </Card>
  );
}
