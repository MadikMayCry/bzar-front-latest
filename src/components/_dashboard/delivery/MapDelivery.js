import React from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
// import './Map.css';
import MarkerClusterGroup from 'react-leaflet-markercluster';
// material
import { List, ListItem, ListItemText, Typography } from '@material-ui/core';

//
import ORDERS from '_mocks_/orders';
import { MapMarker, createCLusterIcon, getLatLngBounds } from 'utils/leaflet';

function MapComponent() {
  const mapIcon = new MapMarker({ iconUrl: '/static/icons/ic_map_marker.svg' });

  return (
    <MapContainer
      bounds={getLatLngBounds(ORDERS)}
      maxZoom={18}
      style={{
        width: '100%',
        height: '600px'
      }}
    >
      <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <MarkerClusterGroup
        iconCreateFunction={createCLusterIcon}
        spiderLegPolylineOptions={{
          weight: 0,
          opacity: 0
        }}
      >
        {ORDERS.map((o) => (
          <Marker position={[o.latitude, o.longitude]} icon={mapIcon} key={o.id}>
            <Popup>
              <List>
                <ListItem disablePadding>
                  <ListItemText primary="Получатель:" />
                  <Typography noWrap sx={{ my: 0 }}>
                    {o.fullname}
                  </Typography>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemText primary="Телефон:" />
                  <Typography noWrap sx={{ my: 0 }}>
                    {o.phone}
                  </Typography>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemText primary="Адрес:" />
                  <Typography noWrap sx={{ my: 0 }}>
                    {o.address}
                  </Typography>
                </ListItem>
              </List>
              <Typography>{o.comment}</Typography>
            </Popup>
          </Marker>
        ))}
      </MarkerClusterGroup>
    </MapContainer>
  );
}

export default MapComponent;
