import PropTypes from 'prop-types';
//
import { Icon } from '@iconify/react';
import shoppingCartFill from '@iconify/icons-eva/shopping-cart-fill';
import heartFilled from '@iconify/icons-ant-design/heart-filled';
// material
import { experimentalStyled as styled } from '@material-ui/core/styles';
import { Badge } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

// ----------------------------------------------------------------------

const RootStyle = styled('div')(({ theme }) => ({
  zIndex: 999,
  right: 0,
  display: 'grid',
  gap: '20px',
  flexDirection: 'column',
  cursor: 'pointer',
  position: 'fixed',
  alignItems: 'center',
  top: theme.spacing(20),
  padding: theme.spacing(2),
  boxShadow: theme.customShadows.z20,
  color: theme.palette.text.primary,
  backgroundColor: theme.palette.background.paper,
  borderTopLeftRadius: theme.shape.borderRadiusMd,
  borderBottomLeftRadius: theme.shape.borderRadiusMd,
  transition: theme.transitions.create('opacity')
}));

// ----------------------------------------------------------------------

CartWidget.propTypes = {
  toggleDrawer: PropTypes.func
};

export default function CartWidget({ toggleDrawer }) {
  const { cartList, favoriteList } = useSelector((state) => state.cart);
  const navigate = useNavigate();

  const goTo = (path) => navigate(path);

  return (
    <RootStyle>
      <Badge
        showZero
        badgeContent={cartList.length}
        color="primary"
        max={99}
        // onClick={() => goTo('cart')}
        onClick={toggleDrawer(true)}
      >
        <Icon icon={shoppingCartFill} width={24} height={24} />
      </Badge>
      <Badge
        showZero
        badgeContent={favoriteList.length}
        color="secondary"
        max={99}
        onClick={() => goTo('favorites')}
      >
        <Icon icon={heartFilled} width={24} height={24} />
      </Badge>
    </RootStyle>
  );
}
