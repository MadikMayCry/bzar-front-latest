import { useState, ChangeEvent } from 'react';
import {
  Box,
  Typography,
  FormControlLabel,
  Switch,
  Tabs,
  Tab,
  TextField,
  IconButton,
  InputAdornment,
  Avatar,
  List,
  Button,
  Tooltip,
  Divider,
  AvatarGroup,
  ListItemButton,
  ListItemAvatar,
  ListItemText,
  lighten
} from '@material-ui/core';
import { alpha, experimentalStyled as styled } from '@material-ui/core/styles';
import { formatDistance, subMinutes, subHours } from 'date-fns';
import { Icon } from '@iconify/react';
// import SettingsTwoToneIcon from '@mui/icons-material/SettingsTwoTone';
// import SearchTwoToneIcon from '@mui/icons-material/SearchTwoTone';

// import CheckTwoToneIcon from '@mui/icons-material/CheckTwoTone';
// import AlarmTwoToneIcon from '@mui/icons-material/AlarmTwoTone';
import checkmarkFill from '@iconify/icons-eva/checkmark-fill';

import Label from 'components/Label';

import { Link as RouterLink } from 'react-router-dom';

const AvatarSuccess = styled(Avatar)(({ theme }) => ({
  color: theme.palette.success.main,
  width: theme.spacing(8),
  height: theme.spacing(8),

  backgroundColor: theme.palette.success.lighter,
  marginLeft: 'auto',
  marginRight: 'auto'
}));

const MeetingBox = styled(Box)(({ theme }) => {
  console.log(theme);
  return {
    backgroundColor: lighten(theme.palette.alpha.black[10], 0.5),
    borderRadius: '6px',
    margin: `${theme.spacing(2)} 0`,
    padding: theme.spacing(2)
  };
});

const RootWrapper = styled(Box)(({ theme }) => ({
  padding: theme.spacing(2.5)
}));

const ListItemWrapper = styled(ListItemButton)(({ theme }) => ({
  '&.MuiButtonBase-root': {
    margin: theme.spacing(1)
  }
}));

const TabsContainerWrapper = styled(Box)(({ theme }) => ({
  '.MuiTabs-indicator': {
    minHeight: '4px',
    boxShadow: 'none',
    height: '4px',
    border: '0'
  },

  '.MuiTab-root': {
    '&.MuiButtonBase-root': {
      padding: 0,
      color: theme.palette.alpha.black[50],

      marginRight: theme.spacing(3),
      fontSize: theme.typography.pxToRem(16),

      '.MuiTouchRipple-root': {
        display: 'none'
      }
    },

    '&.Mui-selected:hover, &.Mui-selected': {
      color: theme.palette.alpha.black[100]
    }
  }
}));

function SidebarContent() {
  const user = {
    name: 'Catherine Pike',
    avatar: '/static/images/avatars/1.jpg',
    jobtitle: 'Software Developer'
  };

  const [state, setState] = useState({
    invisible: true
  });

  const handleChange = (event) => {
    setState({
      ...state,
      [event.target.name]: event.target.checked
    });
  };

  const [currentTab, setCurrentTab] = useState('all');

  const tabs = [
    { value: 'all', label: 'Все' },
    { value: 'unread', label: 'Непрочитанные' }
  ];

  const handleTabsChange = (event, value) => {
    setCurrentTab(value);
  };

  return (
    <RootWrapper>
      <Box display="flex" alignItems="flex-start">
        <Avatar alt={user.name} src={user.avatar} />
        <Box sx={{ ml: 1.5, flex: 1 }}>
          <Box display="flex" alignItems="flex-start" justifyContent="space-between">
            <Box>
              <Typography variant="h5" noWrap>
                {user.name}
              </Typography>
              <Typography variant="subtitle1" noWrap>
                {user.jobtitle}
              </Typography>
            </Box>
            <IconButton sx={{ p: 1 }} size="small" color="primary">
              <Icon icon={checkmarkFill} />
            </IconButton>
          </Box>
        </Box>
      </Box>

      <TextField
        sx={{ mt: 2, mb: 1 }}
        size="small"
        fullWidth
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <Icon icon={checkmarkFill} />
            </InputAdornment>
          )
        }}
        placeholder="Поиск..."
      />

      <Typography sx={{ mb: 1, mt: 2 }} variant="h3">
        Chats
      </Typography>

      <TabsContainerWrapper>
        <Tabs
          onChange={handleTabsChange}
          value={currentTab}
          variant="scrollable"
          scrollButtons="auto"
          textColor="primary"
          indicatorColor="primary"
        >
          {tabs.map((tab) => (
            <Tab key={tab.value} label={tab.label} value={tab.value} />
          ))}
        </Tabs>
      </TabsContainerWrapper>

      <Box mt={2}>
        {currentTab === 'all' && (
          <List disablePadding component="div">
            <ListItemWrapper selected>
              <ListItemAvatar>
                <Avatar src="/static/images/avatars/1.jpg" />
              </ListItemAvatar>
              <ListItemText
                sx={{ mr: 1 }}
                primaryTypographyProps={{
                  color: 'textPrimary',
                  variant: 'h5',
                  noWrap: true
                }}
                secondaryTypographyProps={{
                  color: 'textSecondary',
                  noWrap: true
                }}
                primary="Zain Baptista"
                secondary="Hey there, how are you today? Is it ok if I call you?"
              />
              <Label color="primary">
                <b>2</b>
              </Label>
            </ListItemWrapper>
            <ListItemWrapper>
              <ListItemAvatar>
                <Avatar src="/static/images/avatars/2.jpg" />
              </ListItemAvatar>
              <ListItemText
                sx={{ mr: 1 }}
                primaryTypographyProps={{
                  color: 'textPrimary',
                  variant: 'h5',
                  noWrap: true
                }}
                secondaryTypographyProps={{
                  color: 'textSecondary',
                  noWrap: true
                }}
                primary="Kierra Herwitz"
                secondary="Hi! Did you manage to send me those documents"
              />
            </ListItemWrapper>
            <ListItemWrapper>
              <ListItemAvatar>
                <Avatar src="/static/images/avatars/1.jpg" />
              </ListItemAvatar>
              <ListItemText
                sx={{ mr: 1 }}
                primaryTypographyProps={{
                  color: 'textPrimary',
                  variant: 'h5',
                  noWrap: true
                }}
                secondaryTypographyProps={{
                  color: 'textSecondary',
                  noWrap: true
                }}
                primary="Craig Vaccaro"
                secondary="Ola, I still haven't received the program schedule"
              />
            </ListItemWrapper>
            <ListItemWrapper>
              <ListItemAvatar>
                <Avatar src="/static/images/avatars/4.jpg" />
              </ListItemAvatar>
              <ListItemText
                sx={{ mr: 1 }}
                primaryTypographyProps={{
                  color: 'textPrimary',
                  variant: 'h5',
                  noWrap: true
                }}
                secondaryTypographyProps={{
                  color: 'textSecondary',
                  noWrap: true
                }}
                primary="Adison Press"
                secondary="I recently did some buying on Amazon and now I'm stuck"
              />
              <Label color="primary">
                <b>8</b>
              </Label>
            </ListItemWrapper>
          </List>
        )}
        {currentTab === 'unread' && (
          <List disablePadding component="div">
            <ListItemWrapper>
              <ListItemAvatar>
                <Avatar src="/static/images/avatars/1.jpg" />
              </ListItemAvatar>
              <ListItemText
                sx={{ mr: 1 }}
                primaryTypographyProps={{
                  color: 'textPrimary',
                  variant: 'h5',
                  noWrap: true
                }}
                secondaryTypographyProps={{
                  color: 'textSecondary',
                  noWrap: true
                }}
                primary="Zain Baptista"
                secondary="Hey there, how are you today? Is it ok if I call you?"
              />
              <Label color="primary">
                <b>2</b>
              </Label>
            </ListItemWrapper>
            <ListItemWrapper>
              <ListItemAvatar>
                <Avatar src="/static/images/avatars/4.jpg" />
              </ListItemAvatar>
              <ListItemText
                sx={{ mr: 1 }}
                primaryTypographyProps={{
                  color: 'textPrimary',
                  variant: 'h5',
                  noWrap: true
                }}
                secondaryTypographyProps={{
                  color: 'textSecondary',
                  noWrap: true
                }}
                primary="Adison Press"
                secondary="I recently did some buying on Amazon and now I'm stuck"
              />
              <Label color="primary">
                <b>8</b>
              </Label>
            </ListItemWrapper>
          </List>
        )}
        {currentTab === 'archived' && (
          <Box pb={3}>
            <Divider sx={{ mb: 3 }} />
            <AvatarSuccess>
              <Icon icon={checkmarkFill} />
            </AvatarSuccess>
            <Typography sx={{ mt: 2, textAlign: 'center' }} variant="subtitle2">
              Hurray! There are no archived chats!
            </Typography>
            <Divider sx={{ mt: 3 }} />
          </Box>
        )}
      </Box>
    </RootWrapper>
  );
}

export default SidebarContent;
