import React from 'react';
import { experimentalStyled as styled } from '@material-ui/core/styles';
import { Stack, Typography, IconButton, Divider } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import plusFill from '@iconify/icons-eva/plus-fill';
import minusFill from '@iconify/icons-eva/minus-fill';
import { useDispatch } from 'react-redux';
import { incrementQuantity, decrementQuantity } from 'features/cart/cartSlice';
import { fCurrency } from 'utils/formatNumber';

const ProductImg = styled('img')({
  width: '64px'
});

CartItem.propTypes = {
  name: PropTypes.string,
  cover: PropTypes.string,
  quantity: PropTypes.number,
  price: PropTypes.number
};

function CartItem({ cover, name, quantity, price }) {
  const dispatch = useDispatch();

  return (
    <>
      <Stack flex direction="row" alignItems="center" sx={{ mb: 3 }}>
        <ProductImg alt={name} src={cover} sx={{ mr: 2 }} />
        <Typography variant="body1">{name}</Typography>
        <Stack flex direction="row" alignItems="center" sx={{ ml: 'auto' }}>
          <IconButton variant="contained" onClick={() => dispatch(decrementQuantity(name))}>
            <Icon icon={minusFill} width={20} height={20} />
          </IconButton>
          {quantity}
          <IconButton color="inherit" onClick={() => dispatch(incrementQuantity(name))}>
            <Icon icon={plusFill} width={20} height={20} />
          </IconButton>
        </Stack>
        <div>{fCurrency(quantity * price)} тг</div>
      </Stack>
      <Divider orientation="horizontal" />
    </>
  );
}

export default CartItem;
