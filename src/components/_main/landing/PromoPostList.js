import PropTypes from 'prop-types';
// material
import { Grid } from '@material-ui/core';
import PromoPostCard from './PromoPostCard';

// ----------------------------------------------------------------------

PromoPostList.propTypes = {
  promos: PropTypes.array.isRequired
};

export default function PromoPostList({ promos, ...other }) {
  return (
    <Grid container spacing={3} {...other}>
      {promos.map((promo, index) => (
        <PromoPostCard key={promo.id} promo={promo} index={index} />
      ))}
    </Grid>
  );
}
