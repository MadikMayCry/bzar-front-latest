import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
// material
import { Box, Card, Link, Typography, Stack } from '@material-ui/core';
import { experimentalStyled as styled } from '@material-ui/core/styles';
// ----------------------------------------------------------------------

const ProductImgStyle = styled('img')({
  top: 0,
  width: '100%',
  height: '100%',
  objectFit: 'contain',
  position: 'absolute'
});

// ----------------------------------------------------------------------

ShopProductCard.propTypes = {
  featured: PropTypes.object
};

export default function ShopProductCard({ featured }) {
  const {
    cover,
    category,
    subcategory
    //  imgOne, imgTwo, imgThree
  } = featured;

  return (
    <Card>
      <Box sx={{ pt: '100px', position: 'relative', margin: '0 40%' }} borderRadius={8}>
        <ProductImgStyle alt={category} src={cover} />
      </Box>

      <Stack spacing={2} sx={{ p: 3 }}>
        <Link to="#" color="inherit" underline="hover" component={RouterLink}>
          <Typography variant="subtitle2" noWrap>
            {category}
          </Typography>
        </Link>

        <Stack direction="row" alignItems="center" justifyContent="space-between">
          <Typography
            component="span"
            variant="body2"
            sx={{
              color: 'text.disabled'
            }}
          >
            {subcategory}
          </Typography>
        </Stack>
      </Stack>
    </Card>
  );
}
