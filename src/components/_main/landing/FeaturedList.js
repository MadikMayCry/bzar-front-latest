import React from 'react';
import { experimentalStyled as styled } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

import ScrollBar from 'components/Scrollbar';
import FEATURED from '_mocks_/featured';
import FeaturedCard from './FeaturedCard';

const RootStyle = styled(Grid)(({ theme }) => ({
  [theme.breakpoints.down('md')]: {
    flexDirection: 'row',
    width: 'auto',
    flexWrap: 'nowrap',
    overflowX: 'scroll',
    paddingBottom: 25
  }
}));

const Item = styled(Grid)(({ theme }) => ({
  [theme.breakpoints.up('xs')]: {
    flex: '1 0 50%'
  },
  [theme.breakpoints.up('sm')]: {
    flex: '1 0 33%'
  },
  [theme.breakpoints.up('md')]: {
    flex: '1 0 100%'
  }
}));

function Featured({ ...other }) {
  return (
    // <ScrollBar>
    <RootStyle container spacing={3} {...other}>
      {FEATURED.map((featured) => (
        <Item key={featured.id} item xs={12} sx={{ mb: 0.5 }}>
          <FeaturedCard featured={featured} />
        </Item>
      ))}
    </RootStyle>
    // </ScrollBar>
  );
}

export default Featured;
