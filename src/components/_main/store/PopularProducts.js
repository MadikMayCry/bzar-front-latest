import {
  Box,
  Typography,
  Card,
  CardHeader,
  Divider,
  Avatar,
  Grid,
  Button
} from '@material-ui/core';
import stores from '_mocks_/stores';
import { Icon } from '@iconify/react';
import roundShoppingCart from '@iconify/icons-ic/round-shopping-cart';
import PRODUCTS from '_mocks_/products';
import { ProductList } from 'components/_dashboard/products';

function Feed() {
  return (
    <Card>
      <CardHeader title="Популярные товары" />
      <Divider sx={{ mt: 2 }} />
      <Box p={2}>
        <ProductList products={PRODUCTS.slice(0, 5)} />
      </Box>
    </Card>
  );
}

export default Feed;
