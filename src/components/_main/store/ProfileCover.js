import PropTypes from 'prop-types';
import { Box, Typography, Card, CardMedia, Button, IconButton, Divider } from '@material-ui/core';
import { experimentalStyled as styled } from '@material-ui/core/styles';
import { Icon } from '@iconify/react';
import closeFill from '@iconify/icons-eva/close-fill';
import roundWebAsset from '@iconify/icons-ic/round-web-asset';
import account from '_mocks_/account';
import roundShoppingCart from '@iconify/icons-ic/round-shopping-cart';

const Input = styled('input')({
  display: 'none'
});

const AvatarWrapper = styled(Card)(({ theme }) => ({
  background: 'red',
  position: 'relative',
  overflow: 'visible',
  display: 'inline-block',
  marginTop: -theme.spacing(9),
  marginLeft: theme.spacing(2)
}));

const ButtonUploadWrapper = styled(Box)(({ theme }) => ({
  position: 'absolute',
  width: theme.spacing(4),
  height: theme.spacing(4),
  bottom: theme.spacing(1),
  right: theme.spacing(1),

  '.MuiIconButton-root': {
    borderRadius: '100%',
    background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    boxShadow: theme.shadows.primary,
    width: theme.spacing(4),
    height: theme.spacing(4),
    padding: '0',

    '&:hover': {
      background: theme.palette.primary.dark
    }
  }
}));

const CardCover = styled(Card)(({ theme }) => ({
  position: 'relative',

  '.MuiCardMedia-root': {
    height: theme.spacing(26)
  }
}));

const CardCoverAction = styled(Box)(({ theme }) => ({
  position: 'absolute',
  right: theme.spacing(2),
  bottom: theme.spacing(2)
}));

const ProfileCover = ({ user }) => (
  <>
    <CardCover>
      <CardMedia image={user.coverImg} />
      <CardCoverAction>
        <Input accept="image/*" id="change-cover" multiple type="file" />
        {/* <label htmlFor="change-cover">
          <Button startIcon={<UploadTwoToneIcon />} variant="contained" component="span">
            Change cover
          </Button>
        </label> */}
      </CardCoverAction>
    </CardCover>
    {/* <AvatarWrapper>
      <Avatar variant="rounded" alt={account.displayName} src={account.photoURL} />
      <ButtonUploadWrapper>
        <Input accept="image/*" id="icon-button-file" name="icon-button-file" type="file" />
        <label htmlFor="icon-button-file">
          <IconButton component="span" color="primary">
            <Icon icon={closeFill} />
          </IconButton>
        </label>
      </ButtonUploadWrapper>
    </AvatarWrapper> */}
    <Box py={2} pl={2} mb={3}>
      <Typography gutterBottom variant="h4">
        {user.name}
      </Typography>
      <Typography variant="subtitle2">{user.description}</Typography>
      <Typography sx={{ py: 2 }} variant="subtitle2" color="text.primary">
        {user.jobtitle} | {user.location} | {user.followers}
      </Typography>
      <Box display={{ xs: 'block', md: 'flex' }} alignItems="center" justifyContent="space-between">
        <Box>
          <Button variant="contained">Написать</Button>
          <Button sx={{ mx: 1 }} variant="outlined">
            Перейти на сайт
          </Button>
          <IconButton color="primary" sx={{ p: 0.5 }}>
            <Icon icon={roundWebAsset} />
          </IconButton>
        </Box>
        <Button
          sx={{ mt: { xs: 2, md: 0 } }}
          variant="text"
          endIcon={<Icon icon={roundShoppingCart} />}
        >
          Все товары
        </Button>
      </Box>
      <Divider
        sx={{
          mt: 2
        }}
      />
    </Box>
  </>
);

ProfileCover.propTypes = {
  // @ts-ignore
  user: PropTypes.object.isRequired
};

export default ProfileCover;
