export { default as ProfileCover } from './ProfileCover';
export { default as PopularProducts } from './PopularProducts';
export { default as SameStores } from './SameStores';
