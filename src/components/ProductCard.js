import PropTypes from 'prop-types';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
// material
import { Box, Card, Link, Typography, Stack, Button, IconButton, Rating } from '@material-ui/core';
import { experimentalStyled as styled } from '@material-ui/core/styles';
//
import { Icon } from '@iconify/react';
import shoppingCartFill from '@iconify/icons-eva/shopping-cart-fill';
import heartFilled from '@iconify/icons-ant-design/heart-filled';
//
import { useDispatch, useSelector } from 'react-redux';
import {
  addToFavorite,
  removeFromFavorite,
  addToCart,
  removeFromCart
} from 'features/cart/cartSlice';
//
import RatingStars from 'components/RatingStars';
// utils
import { fCurrency } from 'utils/formatNumber';
import { getItemIndex } from 'utils/helpers';
//
import Label from 'components/Label';
import ColorPreview from 'components/ColorPreview';

// ----------------------------------------------------------------------

const ProductImgStyle = styled('img')({
  top: 0,
  width: '100%',
  height: '100%',
  objectFit: 'cover',
  position: 'absolute'
});

// ----------------------------------------------------------------------

ShopProductCard.propTypes = {
  product: PropTypes.object
};

export default function ShopProductCard({ product }) {
  const { name, cover, price, status, priceSale, rating } = product;
  //
  const { favoriteList, cartList } = useSelector((state) => state.cart);
  //
  const favoriteIndex = getItemIndex(favoriteList, name);
  const isInFavorite = favoriteIndex >= 0;
  //
  const cartIndex = getItemIndex(cartList, name);
  const isInCart = cartIndex >= 0;

  const navigate = useNavigate();

  const dispatch = useDispatch();

  const addFavorite = () => {
    dispatch(addToFavorite(product));
  };

  const removeFavorite = () => {
    dispatch(removeFromFavorite(name));
  };

  const addCart = () => {
    dispatch(addToCart({ ...product, quantity: 1 }));
  };

  const removeCart = () => {
    dispatch(removeFromCart(name));
  };

  const clickFavorite = () => {
    if (isInFavorite) removeFavorite();
    else {
      addFavorite();
    }
  };

  const clickCart = () => {
    if (isInCart) removeCart();
    else {
      addCart();
    }
  };

  // navigate(`/catalog/${p}`);
  const goToProduct = () => navigate(`/product-details/${name}`);

  return (
    <Card>
      <Box sx={{ pt: '100%', position: 'relative' }}>
        {status && (
          <Label
            variant="filled"
            color={(status === 'sale' && 'error') || 'info'}
            sx={{
              zIndex: 9,
              top: 16,
              right: 16,
              position: 'absolute',
              textTransform: 'uppercase'
            }}
          >
            {status}
          </Label>
        )}
        <ProductImgStyle alt={name} src={cover} />
      </Box>

      <Stack spacing={2} sx={{ p: 3 }}>
        <Link to="#" color="inherit" underline="hover" component={RouterLink}>
          <Typography variant="subtitle2" noWrap>
            {name}
          </Typography>
        </Link>

        <Stack direction="row" alignItems="center" justifyContent="space-between">
          {/* <ColorPreview colors={colors} /> */}
          <Typography variant="subtitle1">
            <Typography
              component="span"
              variant="body1"
              sx={{
                color: 'text.disabled',
                textDecoration: 'line-through'
              }}
            >
              {priceSale && `${fCurrency(priceSale)}`}
            </Typography>
            &nbsp;
            {fCurrency(price)} тг
          </Typography>
        </Stack>
        <Rating
          sx={{ justifyContent: 'center' }}
          size="large"
          name="simple-controlled"
          value={rating}
          // onChange={(event, newValue) => {
          //   setValue(newValue);
          // }}
        />
        {/* <RatingStars rating={4} /> */}
        <Stack direction="row" alignItems="center" justifyContent="space-between">
          <Stack direction="row" alignItems="center" justifyContent="space-between">
            <IconButton
              variant="contained"
              color={isInCart ? 'primary' : 'inherit'}
              onClick={clickCart}
            >
              <Icon icon={shoppingCartFill} width={24} height={24} />
            </IconButton>
            <IconButton
              variant="contained"
              color={isInFavorite ? 'secondary' : 'inherit'}
              onClick={clickFavorite}
            >
              <Icon icon={heartFilled} width={24} height={24} />
            </IconButton>
          </Stack>
          <Button variant="contained" onClick={goToProduct}>
            Подробнее
          </Button>
        </Stack>
      </Stack>
    </Card>
  );
}
