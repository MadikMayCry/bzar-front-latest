import React from 'react';
import PropTypes from 'prop-types';
import Rate from 'rc-rate';
import { experimentalStyled as styled } from '@material-ui/core/styles';

RatingStars.propTypes = {
  rating: PropTypes.number,
  size: PropTypes.string,
  allowHalf: PropTypes.bool,
  onClick: PropTypes.func
};

function RatingStars({ rating, size = '32', allowHalf = false, onClick }) {
  // let rating = Math.round(Math.random() * 5 * 10) / 10;

  return <StyledRate defaultValue={rating} size={size} allowHalf={allowHalf} onChange={onClick} />;
}

const StyledRate = styled(Rate)(() => ({
  display: 'block',
  margin: '10px auto 0!important',
  '& .rc-rate-star': {
    marginRight: '10px',
    fontSize: '32px',
    margin: '0 5px'
  },
  '& .rc-rate-star-full .rc-rate-star-second': {
    textShadow: '0px 2px 4px rgb(81 64 64 / 0.25)'
  },
  '& .rc-rate-star div': {
    outline: 'none !important'
  }
}));

export default RatingStars;

// Props
// count	number	5	Star numbers
// value	number	-	Controlled value
// defaultValue	number	0	Initial value
// allowHalf	boolean	false	Support half star
// allowClear	boolean	true	Reset when click again
// style	object	{}
// onChange	function	(value) => {}	onChange will be triggered when click
// onHoverChange	function	(value) => {}	onHoverChange will be triggered when hover on stars
// character	ReactNode | (props) => ReactNode	★	The each character of rate
// disabled	boolean	false
// direction	string	ltr	The direction of rate
