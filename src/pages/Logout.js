import React, { Component } from 'react';
import userManager from 'utils/userManager';

class LogoutPage extends Component {
  componentDidMount() {
    userManager.signoutRedirect();
  }

  render() {
    return (
      <div>
        <h3>Log out</h3>
      </div>
    );
  }
}

export default LogoutPage;
