import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useFormik } from 'formik';
// material
import { Container, Stack } from '@material-ui/core';
// components
import Page from 'components/Page';
import {
  ProductSort,
  ProductList,
  ProductCartWidget,
  ProductFilterSidebar
} from 'components/_dashboard/products';
//
import PRODUCTS from '_mocks_/products';
import db from 'utils/db';

// ----------------------------------------------------------------------

export default function EcommerceShop() {
  const { catId, id } = useParams();
  //
  const [category, setCategory] = useState({});
  const [subCategory, setSubCategory] = useState({
    products: []
  });
  //
  const [openFilter, setOpenFilter] = useState(false);

  useEffect(() => {
    const catObj = db.categories.find((cat) => cat.code.toString() === catId);

    const subCat = catObj.subCategories.find((subCat) => subCat.code.toString() === id);

    setCategory(catObj);
    setSubCategory(subCat);
  }, [catId, id]);

  const formik = useFormik({
    initialValues: {
      gender: '',
      category: '',
      colors: '',
      priceRange: [0, 10000],
      rating: ''
    },
    onSubmit: (values) => {
      console.log(values);
      setOpenFilter(false);
    }
  });

  const { resetForm, handleSubmit } = formik;

  const handleOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleCloseFilter = () => {
    setOpenFilter(false);
  };

  return (
    <Page title="Dashboard: Products | Bzar kz">
      <Container>
        {/* <Typography variant="h4" sx={{ mb: 5 }}>
          Товары
        </Typography> */}

        <Stack
          direction="row"
          flexWrap="wrap-reverse"
          alignItems="center"
          justifyContent="flex-end"
          sx={{ mb: 5 }}
        >
          <Stack direction="row" spacing={1} flexShrink={0} sx={{ my: 1 }}>
            <ProductFilterSidebar
              formik={formik}
              isOpenFilter={openFilter}
              onResetFilter={resetForm}
              onOpenFilter={handleOpenFilter}
              onCloseFilter={handleCloseFilter}
              handleSubmit={handleSubmit}
            />
            <ProductSort />
          </Stack>
        </Stack>

        <ProductList products={subCategory.products} />
        {/* <ProductCartWidget /> */}
      </Container>
    </Page>
  );
}
