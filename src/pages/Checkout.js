import * as Yup from 'yup';
import { useState } from 'react';
import { Icon } from '@iconify/react';
import { useFormik, Form, FormikProvider } from 'formik';
import eyeFill from '@iconify/icons-eva/eye-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import { useNavigate } from 'react-router-dom';
// material
import {
  Typography,
  Stack,
  TextField,
  IconButton,
  InputAdornment,
  Container
} from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';

// components
import Page from 'components/Page';

const PAYBOX_TOKEN = 'QxMNWrKt1B2mD4WXgurRPxChlPyXCWdc';

// ----------------------------------------------------------------------

// Paybox
(function (p, a, y, b, o, x) {
  o = p.createElement(a);
  [x] = p.getElementsByTagName(a);
  o.async = 1;
  o.src = `https://widget.paybox.money/v1/paybox/pbwidget.js? ${1 * new Date()}`;
  x.parentNode.insertBefore(o, x);
})(document, 'script');

export default function RegisterForm() {
  const navigate = useNavigate();

  const RegisterSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(2, 'Слишком Коротко!')
      .max(50, 'Слишком длинно!')
      .required('Имя обязательно'),
    lastName: Yup.string()
      .min(2, 'Слишком Коротко!')
      .max(50, 'Слишком длинно!')
      .required('Фамилия обязательно'),
    email: Yup.string().email('Email must be a valid email address').required('Email обязателен'),
    address: Yup.string()
      .min(2, 'Слишком Коротко!')
      .max(50, 'Слишком длинно!')
      .required('Адрес обязателен'),
    promocode: Yup.string().min(2, 'Слишком Коротко!').max(50, 'Слишком длинно!'),
    postalCode: Yup.string().min(2, 'Слишком Коротко!').required('Индекс обязателен'),
    phoneNumber: Yup.string().min(2, 'Слишком Коротко!').required('Номер телефона обязателен')
  });

  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      address: '',
      postalCode: '',
      phoneNumber: ''
    },
    validationSchema: RegisterSchema,
    onSubmit: () => {
      pay();
      // navigate('/dashboard', { replace: true });
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  function pay(amount = 200) {
    const data = {
      token: PAYBOX_TOKEN,
      payment: {
        order: '1',
        amount: 200,
        currency: 'KZT',
        description: 'Описание заказа',
        expires_at: '2020-12-12 00:00:00',
        test: 1,
        param1: 'string',
        param2: 'string',
        param3: 'string',
        options: {
          callbacks: {
            result_url: 'https://my-domain.com/result',
            check_url: 'https://my-domain.com/check'
          },
          custom_params: {},
          user: {
            email: 'user@test.com',
            phone: '77777777777'
          },
          receipt_positions: [
            {
              count: 2,
              name: 'Коврик для мыши',
              tax_type: 3,
              price: 1000
            },
            {
              count: 2,
              name: 'Розетка',
              tax_type: 3,
              price: 1000
            }
          ]
        }
      },
      successCallback: (payment) => {},
      errorCallback: (payment) => {}
    };

    const paybox = new window.PayBox(data);
    paybox.create();
  }

  return (
    <Page title="Bzar kz: Корзина">
      <Container>
        <Typography variant="h5" gutterBottom sx={{ mb: 3 }}>
          Корзина
        </Typography>

        <FormikProvider value={formik}>
          <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
            <Stack spacing={3}>
              <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
                <TextField
                  fullWidth
                  label="Имя"
                  {...getFieldProps('firstName')}
                  error={Boolean(touched.firstName && errors.firstName)}
                  helperText={touched.firstName && errors.firstName}
                />

                <TextField
                  fullWidth
                  label="Фамилия"
                  {...getFieldProps('lastName')}
                  error={Boolean(touched.lastName && errors.lastName)}
                  helperText={touched.lastName && errors.lastName}
                />
              </Stack>
              <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
                <TextField
                  fullWidth
                  label="Адрес"
                  {...getFieldProps('address')}
                  error={Boolean(touched.address && errors.address)}
                  helperText={touched.address && errors.address}
                />
                <TextField
                  fullWidth
                  label="Почтовый индекс"
                  {...getFieldProps('postalCode')}
                  error={Boolean(touched.postalCode && errors.postalCode)}
                  helperText={touched.postalCode && errors.postalCode}
                />
              </Stack>
              <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
                <TextField
                  fullWidth
                  label="Номер телефона"
                  {...getFieldProps('phoneNumber')}
                  error={Boolean(touched.phoneNumber && errors.phoneNumber)}
                  helperText={touched.phoneNumber && errors.phoneNumber}
                />
                <TextField
                  fullWidth
                  autoComplete="username"
                  type="email"
                  label="Email"
                  {...getFieldProps('email')}
                  error={Boolean(touched.email && errors.email)}
                  helperText={touched.email && errors.email}
                />
              </Stack>
              <TextField
                fullWidth
                label="Промокод"
                {...getFieldProps('promocode')}
                error={Boolean(touched.promocode && errors.promocode)}
                helperText={touched.promocode && errors.promocode}
              />
              <LoadingButton
                fullWidth={false}
                type="submit"
                variant="contained"
                loading={isSubmitting}
              >
                Оплатить
              </LoadingButton>
            </Stack>
          </Form>
        </FormikProvider>
      </Container>
    </Page>
  );
}
