// material
import { Container, Grid, Typography, Button, Stack } from '@material-ui/core';
// components
import Page from 'components/Page';

import { ProfileCover, PopularProducts, SameStores } from 'components/_main/store';
import { Link as RouterLink } from 'react-router-dom';
//
import PRODUCTS from '_mocks_/products';
import POSTS from '_mocks_/blog';

// ----------------------------------------------------------------------

export default function EcommerceShop() {
  const user = {
    savedCards: 7,
    name: 'Technodom kz',
    coverImg: '/static/images/placeholders/covers/7.jpg',
    avatar: '/static/images/avatars/4.jpg',
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage",
    jobtitle: 'Категория',
    location: 'Категория',
    followers: 'Категория'
  };

  return (
    <Page title="Technodom: Bzar kz">
      <Container sx={{ mt: 3 }} maxWidth="lg">
        <Grid container direction="row" justifyContent="center" alignItems="stretch" spacing={3}>
          <Grid item xs={12}>
            <ProfileCover user={user} />
          </Grid>
          <Grid item xs={12}>
            <PopularProducts />
          </Grid>
          <Grid item xs={12}>
            <SameStores />
          </Grid>
          {/* <Grid item xs={12} md={8}>
            <Feed />
          </Grid>
          <Grid item xs={12} md={4}>
            <PopularTags />
          </Grid>
          <Grid item xs={12} md={7}>
            <MyCards />
          </Grid>
          <Grid item xs={12} md={5}>
            <Addresses />
          </Grid> */}
        </Grid>
      </Container>
    </Page>
  );
}
