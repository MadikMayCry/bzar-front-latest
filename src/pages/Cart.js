// material
import { Container, Grid, Typography, Button, Stack } from '@material-ui/core';
//
import { Icon } from '@iconify/react';
import roundClearAll from '@iconify/icons-ic/round-clear-all';
// components
import Page from 'components/Page';
import CartItem from 'components/_main/cart/CartItem';
import { clearCart } from 'features/cart/cartSlice';

import { Link as RouterLink } from 'react-router-dom';

//
import { useSelector, useDispatch } from 'react-redux';

// ----------------------------------------------------------------------

export default function Cart() {
  const dispatch = useDispatch();
  const clearClick = () => dispatch(clearCart());

  const { cartList } = useSelector((state) => state.cart);
  return (
    <Page title="Bzar kz: Корзина">
      <Container>
        <Typography variant="h5" gutterBottom sx={{ mb: 3 }}>
          Корзина
        </Typography>
        <Grid container spacing={4} direction="column">
          {cartList.map((cartItem) => (
            <Grid item xs={12} key={cartItem}>
              <CartItem {...cartItem} />
            </Grid>
          ))}
        </Grid>
        <Stack flex direction="row" sx={{ p: 3 }} justifyContent="space-between">
          <Button
            color="inherit"
            variant="outlined"
            onClick={clearClick}
            startIcon={<Icon icon={roundClearAll} />}
          >
            Очистить
          </Button>
          <Button component={RouterLink} to="/checkout" variant="contained">
            Оформить заказ
          </Button>
        </Stack>
      </Container>
    </Page>
  );
}
