import { useRef, useEffect } from 'react';
// material
import { Container, Grid, Typography, Button, Stack, Box } from '@material-ui/core';
// components
import Page from 'components/Page';
import { alpha, experimentalStyled as styled } from '@material-ui/core/styles';
import { ProfileCover, PopularProducts, SameStores } from 'components/_main/store';
import ScrollBar from 'components/Scrollbar';

import { Link as RouterLink } from 'react-router-dom';
import { SidebarContent, TopBarContent } from 'components/_dashboard/messenger';
// ----------------------------------------------------------------------

const RootWrapper = styled(Box)(() => ({
  height: '100%',
  display: 'flex'
}));

const Sidebar = styled(Box)(({ theme }) => {
  const i = null;
  return {
    width: '350px',
    background: theme.palette.alpha.white[100],
    borderRight: `${theme.palette.alpha.black[10]} solid 1px`
  };
});

const ChatWindow = styled(Box)(() => ({
  width: '100%',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  flex: 1
}));

const ChatTopBar = styled(Box)(({ theme }) => ({
  background: theme.palette.alpha.white[100],
  borderBottom: `${theme.palette.alpha.black[10]} solid 1px`,
  padding: theme.spacing(3)
}));

const ChatMain = styled(Box)(() => ({
  flex: 1
}));

const ChatBottomBar = styled(Box)(({ theme }) => ({
  padding: theme.spacing(3)
}));

export default function EcommerceShop() {
  const ref = useRef(null);

  useEffect(() => {
    if (ref.current) {
      ref.current.scrollToBottom();
    }
  });

  return (
    <Page title="Диалог Technodom: Bzar kz">
      <Container sx={{ mt: 3 }}>
        <RootWrapper>
          <Sidebar>
            <ScrollBar sx={{ height: { xs: 340, sm: 'auto' } }}>
              <SidebarContent />
            </ScrollBar>
          </Sidebar>
          <ChatWindow>
            <ChatTopBar>
              <TopBarContent />
            </ChatTopBar>
            <ChatMain>
              <ScrollBar ref={ref} autoHide>
                {/* <ChatContent /> */}
              </ScrollBar>
            </ChatMain>
            <ChatBottomBar>{/* <BottomBarContent /> */}</ChatBottomBar>
          </ChatWindow>
        </RootWrapper>
      </Container>
    </Page>
  );
}
