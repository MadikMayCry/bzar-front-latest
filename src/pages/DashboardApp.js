// material
import { Box, Grid, Container, Typography } from '@material-ui/core';
// components
import Page from '../components/Page';
import {
  // AppTasks,
  // AppNewUsers,
  // AppBugReports,
  AppItemOrders,
  AppNewsUpdate,
  AppWeeklySales,
  AppOrderTimeline,
  AppCurrentVisits,
  AppWebsiteVisits,
  // AppTrafficBySite,
  // AppCurrentSubject,
  AppConversionRates
} from '../components/_dashboard/app';

// ----------------------------------------------------------------------

export default function DashboardApp() {
  return (
    <Page title="Dashboard | Bzar kz">
      <Container maxWidth="xl">
        <Box sx={{ pb: 5 }}>
          <Typography variant="h4">Добро пожаловать</Typography>
        </Box>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} md={6}>
            <AppWeeklySales />
          </Grid>
          {/* <Grid item xs={12} sm={6} md={4}>
            <AppNewUsers />
          </Grid> */}
          <Grid item xs={12} sm={6} md={6}>
            <AppItemOrders />
          </Grid>

          <Grid item xs={12} md={12} lg={6}>
            <AppWebsiteVisits />
          </Grid>

          <Grid item xs={12} md={12} lg={6}>
            <AppConversionRates />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppCurrentVisits />
          </Grid>

          {/* <Grid item xs={12} md={6} lg={4}> */}
          {/*  <AppCurrentSubject /> */}
          {/* </Grid> */}

          <Grid item xs={12} md={6} lg={4}>
            <AppOrderTimeline />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppNewsUpdate />
          </Grid>

          {/* <Grid item xs={12} md={6} lg={4}>
            <AppTrafficBySite />
          </Grid> */}

          {/* <Grid item xs={12} md={6} lg={4}> */}
          {/*  <AppTasks /> */}
          {/* </Grid> */}
        </Grid>
      </Container>
    </Page>
  );
}
