// material
import { Grid, Container, Stack, Typography } from '@material-ui/core';
// components
import DeliveryPointMap from 'components/_dashboard/delivery/DeliveryPointMap';
import Page from 'components/Page';
import { BlogPostCard, BlogPostsSort, BlogPostsSearch } from 'components/_dashboard/blog';
//
import { MapDelivery } from 'components/_dashboard/delivery';

// ----------------------------------------------------------------------

const SORT_OPTIONS = [
  { value: 'latest', label: 'Latest' },
  { value: 'popular', label: 'Popular' },
  { value: 'oldest', label: 'Oldest' }
];

// ----------------------------------------------------------------------

export default function Courier() {
  return (
    <Page title="Bzar kz: Новости">
      <Container maxWidth="xl">
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            Доставка
          </Typography>
        </Stack>

        <Stack mb={5} direction="row" alignItems="center" justifyContent="space-between">
          <MapDelivery />
          {/* <BlogPostsSearch posts={POSTS} /> */}
          {/* <BlogPostsSort options={SORT_OPTIONS} /> */}
        </Stack>
      </Container>
    </Page>
  );
}
