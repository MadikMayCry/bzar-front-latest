import { Icon } from '@iconify/react';
import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink } from 'react-router-dom';
// material
import { Container, Stack, Typography, Button } from '@material-ui/core';
// components
import Page from 'components/Page';
import { ProductList } from 'components/_dashboard/products';
//
import PRODUCTS from '_mocks_/products';

// ----------------------------------------------------------------------

export default function EcommerceShop() {
  return (
    <Page title="Dashboard: Products | Bzar kz">
      <Container maxWidth="xl">
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            Товары
          </Typography>
          <Button
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Icon icon={plusFill} />}
          >
            Добавить товар
          </Button>
        </Stack>

        <ProductList products={PRODUCTS} />
      </Container>
    </Page>
  );
}
