import React from 'react';
import { CallbackComponent } from 'redux-oidc';
import userManager from 'utils/userManager';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

const CallbackPage = () => {
  const navigate = useNavigate();
  const { redirectUrl } = useSelector((state) => state.stuff);
  return (
    <CallbackComponent
      userManager={userManager}
      successCallback={(e) => {
        console.log(e);
        // Отсюда перекидывать на любую приватную страницу куда первоначально хотел юзер
        navigate(redirectUrl);
      }}
      errorCallback={(error) => {
        navigate('/');
        console.error(error);
      }}
    >
      <div>Перенаправляем...</div>
    </CallbackComponent>
  );
};

export default CallbackPage;
