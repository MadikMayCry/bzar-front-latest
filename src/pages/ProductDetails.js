import { useEffect, useState } from 'react';
// material
import { experimentalStyled as styled } from '@material-ui/core/styles';
import {
  Box,
  Grid,
  Container,
  Divider,
  Stack,
  Typography,
  Button,
  Rating,
  Link
} from '@material-ui/core';
import { Link as RouterLink, useParams } from 'react-router-dom';

// icons
import { Icon } from '@iconify/react';
import shoppingCartFill from '@iconify/icons-eva/shopping-cart-fill';
import heartFilled from '@iconify/icons-ant-design/heart-filled';
import messageCircleFill from '@iconify/icons-eva/message-circle-fill';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  addToFavorite,
  removeFromFavorite,
  addToCart,
  removeFromCart
} from 'features/cart/cartSlice';

// components
import Page from 'components/Page';

//
import { fCurrency } from 'utils/formatNumber';
import { findById, getItemIndex } from 'utils/helpers';
import db from 'utils/db';

// ----------------------------------------------------------------------

const SmallPhoto = styled('img')(({ theme }) => ({
  maxWidth: '25%',
  border: '2px solid #eee',
  borderRadius: '6px',
  padding: '5px',
  marginRight: '10px',
  cursor: 'pointer',
  objectFit: 'contain',
  '&:hover': {
    border: '2px solid #ddd'
  }
}));

export default function ProductDetails() {
  // redux
  const dispatch = useDispatch();
  const { cartList, favoriteList } = useSelector((state) => state.cart);
  //
  const { productId } = useParams();

  let product = { name, cover, description, lorem, smallPhotos: [] };

  product = findById(db, productId);

  const [selectedPhoto, setSelectedPhoto] = useState(0);

  //
  const favoriteIndex = getItemIndex(favoriteList, productId);
  const isInFavorite = favoriteIndex >= 0;
  //
  const cartIndex = getItemIndex(cartList, productId);
  const isInCart = cartIndex >= 0;

  const addFavorite = () => {
    dispatch(addToFavorite(product));
  };

  const removeFavorite = () => {
    dispatch(removeFromFavorite(name));
  };

  const addCart = () => {
    dispatch(addToCart({ ...product, quantity: 1 }));
  };

  const removeCart = () => {
    dispatch(removeFromCart(name));
  };

  const clickFavorite = () => {
    if (isInFavorite) removeFavorite();
    else {
      addFavorite();
    }
  };

  const clickCart = () => {
    if (isInCart) removeCart();
    else {
      addCart();
    }
  };

  const { name, cover, description, lorem, smallPhotos, price, priceSale, rating, comment, store } =
    product;

  return (
    <Page title="Bzar kz: Детали продукта">
      <Container>
        {/* <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            {product.name}
          </Typography>
        </Stack> */}
        <Grid container spacing={4}>
          <Grid item xs={12} md={7}>
            <Stack justifyContent="center">
              <img alt={name} src={smallPhotos[selectedPhoto]} />
              <Stack direction="row" alignItems="center" justifyContent="center" mt={2}>
                {smallPhotos.map((s, index) => (
                  <SmallPhoto
                    alt={s}
                    key={index}
                    src={s}
                    sx={{ borderColor: selectedPhoto === index && 'black' }}
                    onClick={() => setSelectedPhoto(index)}
                    // loading="lazy"
                  />
                ))}
              </Stack>
            </Stack>
          </Grid>
          <Grid item xs={12} md={5}>
            <Typography variant="h4" gutterBottom>
              {name}
            </Typography>
            <Typography>{description}</Typography>
            <Divider orientation="horizontal" sx={{ my: 2 }} />
            <Stack>
              <Typography variant="subtitle1" sx={{ mb: 2 }}>
                Цена:
              </Typography>
              <Typography variant="subtitle1">
                <Typography
                  component="span"
                  variant="body1"
                  sx={{
                    color: 'text.disabled',
                    textDecoration: 'line-through'
                  }}
                >
                  {priceSale && `${fCurrency(priceSale)}`}
                </Typography>
                &nbsp;
                {fCurrency(price)} тг
              </Typography>
            </Stack>
            <Divider orientation="horizontal" sx={{ my: 2 }} />
            <Stack>
              <Typography variant="subtitle1" sx={{ mb: 2 }}>
                Отзывы:
              </Typography>
              <Stack direction="row" justifyContent="space-between">
                <Rating name="simple-controlled" value={rating} />
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center'
                  }}
                >
                  <Icon icon={messageCircleFill} width={20} height={20} />

                  <Typography ml={1} variant="body1">
                    {comment}
                  </Typography>
                </Box>
              </Stack>
            </Stack>
            <Divider orientation="horizontal" sx={{ my: 2 }} />
            <Stack direction="row" spacing={2}>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                onClick={clickCart}
                startIcon={<Icon icon={shoppingCartFill} />}
              >
                {isInCart ? 'Убрать из корзины' : 'Добавить в корзину'}
              </Button>

              <Button
                fullWidth
                variant="contained"
                color="info"
                startIcon={<Icon icon={heartFilled} />}
                onClick={clickFavorite}
              >
                {isInFavorite ? 'Убрать из избранного' : 'Добавить в избранное'}
              </Button>
            </Stack>
            <Divider orientation="horizontal" sx={{ my: 2 }} />
            <Typography variant="subtitle1" sx={{ mb: 2 }}>
              Продавец:
            </Typography>
            <Link to="/store" color="inherit" underline="hover" component={RouterLink}>
              <Typography variant="subtitle2" noWrap>
                Технодом
              </Typography>
            </Link>
            <Divider orientation="horizontal" sx={{ my: 2 }} />
            <Stack>
              <Typography variant="subtitle1" sx={{ mb: 2 }}>
                Описание:
              </Typography>
              <Typography style={{ whiteSpace: 'pre-line' }} variant="body1">
                {lorem}
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
}
