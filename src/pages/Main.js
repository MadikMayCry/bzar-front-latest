// material
import { Container, Grid, Typography, Button, Stack } from '@material-ui/core';
// components
import Page from 'components/Page';

import { FeaturedList, ProductList, PromoPostList } from 'components/_main/landing';
import { Link as RouterLink } from 'react-router-dom';
//
import PRODUCTS from '_mocks_/products';
import POSTS from '_mocks_/blog';

// ----------------------------------------------------------------------

export default function EcommerceShop() {
  return (
    <Page title="Dashboard: Products | Bzar kz">
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12} md={2}>
            <Typography variant="h5" gutterBottom sx={{ mb: 3 }}>
              Лучшие магазины
            </Typography>
            <FeaturedList />
          </Grid>
          <Grid item xs={12} md={10}>
            <Stack direction="row" alignItems="center" justifyContent="space-between">
              <Typography variant="h5" gutterBottom sx={{ mb: 3 }}>
                Лучшие предложения
              </Typography>
              {/* <Button variant="inherit" component={RouterLink} to="/catalog">
                Перейти к каталогу
              </Button> */}
            </Stack>
            <ProductList products={PRODUCTS.slice(0, 8)} />
            <Typography variant="h5" gutterBottom sx={{ my: 3 }}>
              Промоакции
            </Typography>
            <PromoPostList promos={POSTS.slice(0, 2)} />
          </Grid>
        </Grid>

        {/* <ProductCartWidget /> */}
      </Container>
    </Page>
  );
}
