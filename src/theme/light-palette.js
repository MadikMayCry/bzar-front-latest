import { alpha } from '@material-ui/core/styles';

// ----------------------------------------------------------------------

function createGradient(color1, color2) {
  return `linear-gradient(to bottom, ${color1}, ${color2})`;
}

// SETUP COLORS
const GREY = {
  0: '#FFFFFF',
  100: '#F9FAFB',
  200: '#F4F6F8',
  300: '#DFE3E8',
  400: '#C4CDD5',
  500: '#919EAB',
  600: '#637381',
  700: '#454F5B',
  800: '#212B36',
  900: '#161C24',
  500_8: alpha('#919EAB', 0.08),
  500_12: alpha('#919EAB', 0.12),
  500_16: alpha('#919EAB', 0.16),
  500_24: alpha('#919EAB', 0.24),
  500_32: alpha('#919EAB', 0.32),
  500_48: alpha('#919EAB', 0.48),
  500_56: alpha('#919EAB', 0.56),
  500_80: alpha('#919EAB', 0.8)
};

const PRIMARY = {
  lighter: '#fed2cd',
  light: '#f98e96',
  main: '#F65E69',
  dark: '#ac424a',
  darker: '#62262a',
  contrastText: '#fff'
};
const SECONDARY = {
  lighter: '#fdcec2',
  light: '#fcaa94',
  main: '#FA8566',
  dark: '#af5d47',
  darker: '#643529',
  contrastText: '#fff'
};
const INFO = {
  lighter: '#bee0ff',
  light: '#8ec8ff',
  main: '#1890FF',
  dark: '#417cb3',
  darker: '#254766',
  contrastText: '#fff'
};
const SUCCESS = {
  lighter: '#b4dda7',
  light: '#7bc465',
  main: '#43ab23',
  dark: '#2f7819',
  darker: '#1b440e',
  contrastText: GREY[800]
};
const WARNING = {
  lighter: '#FFF7CD',
  light: '#FFE16A',
  main: '#FFC107',
  dark: '#B78103',
  darker: '#7A4F01',
  contrastText: GREY[800]
};
const ERROR = {
  lighter: '#FFE7D9',
  light: '#FFA48D',
  main: '#FF4842',
  dark: '#B72136',
  darker: '#7A0C2E',
  contrastText: '#fff'
};

const GRADIENTS = {
  primary: createGradient(PRIMARY.light, PRIMARY.main),
  info: createGradient(INFO.light, INFO.main),
  success: createGradient(SUCCESS.light, SUCCESS.main),
  warning: createGradient(WARNING.light, WARNING.main),
  error: createGradient(ERROR.light, ERROR.main)
};

const palette = {
  mode: 'light',
  common: { black: '#000', white: '#fff' },
  primary: { ...PRIMARY },
  secondary: { ...SECONDARY },
  info: { ...INFO },
  success: { ...SUCCESS },
  warning: { ...WARNING },
  error: { ...ERROR },
  grey: GREY,
  gradients: GRADIENTS,
  divider: GREY[500_24],
  text: { primary: GREY[800], secondary: GREY[600], disabled: GREY[500] },
  background: { paper: '#fff', default: '#fff', neutral: GREY[200] },
  action: {
    active: GREY[600],
    hover: GREY[500_8],
    selected: GREY[500_16],
    disabled: GREY[500_80],
    disabledBackground: GREY[500_24],
    focus: GREY[500_24],
    hoverOpacity: 0.08,
    disabledOpacity: 0.48
  },
  alpha: {
    white: {
      5: alpha('#fff', 0.02),
      10: alpha('#fff', 0.1),
      30: alpha('#fff', 0.3),
      50: alpha('#fff', 0.5),
      70: alpha('#fff', 0.7),
      100: '#fff'
    },
    trueWhite: {
      5: alpha('#fff', 0.02),
      10: alpha('#fff', 0.1),
      30: alpha('#fff', 0.3),
      50: alpha('#fff', 0.5),
      70: alpha('#fff', 0.7),
      100: '#fff'
    },
    black: {
      5: alpha('#000', 0.02),
      10: alpha('#000', 0.1),
      30: alpha('#000', 0.3),
      50: alpha('#000', 0.5),
      70: alpha('#000', 0.7),
      100: '#000'
    }
  },
  general: {
    reactFrameworkColor: '#00D8FF',
    borderRadiusSm: '4px',
    borderRadius: '6px',
    borderRadiusLg: '10px',
    borderRadiusXl: '18px'
  }
};

export default palette;
