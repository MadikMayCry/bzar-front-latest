import faker from 'faker';

// ----------------------------------------------------------------------

export const FEATURED_NAME = ['Женщинам'];

// ----------------------------------------------------------------------

const featured = [...Array(10)].map(() => ({
  id: faker.datatype.uuid(),
  cover: '/static/mock-images/featured/lumene_logo.png',
  imgOne: '/static/mock-images/featured/one.png',
  imgTwo: '/static/mock-images/featured/two.png',
  imgThree: '/static/mock-images/featured/three.png',
  name: 'Женщинам',
  category: faker.commerce.productAdjective(),
  subcategory: 'Для Здоровья'
}));

export default featured;
