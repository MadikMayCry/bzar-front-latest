import faker from 'faker';
import { sample } from 'lodash';
// utils
import { mockImgAvatar } from '../utils/mockImages';

const orders = [...Array(24)].map((_, index) => ({
  id: faker.datatype.uuid(),
  avatarUrl: mockImgAvatar(index + 1),
  orderDate: faker.date.past(),
  comment: faker.lorem.paragraph(),
  fullname: `${faker.name.firstName()} ${faker.name.lastName()}`,
  address: faker.address.streetAddress(),
  longitude: sample([76.87691, 76.95696, 76.8803, 76.8175]),
  latitude: sample([43.2445, 43.24999, 43.24294, 43.25302]),
  phone: faker.phone.phoneNumber(),

  status: sample(['Доставлен', 'Ожидает доставки']),
  isPayed: faker.datatype.boolean()
}));

export default orders;
