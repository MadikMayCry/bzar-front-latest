import faker from 'faker';
// utils
import { mockImgAvatar } from '../utils/mockImages';

// ----------------------------------------------------------------------

const stores = [...Array(6)].map((_, index) => ({
  id: faker.datatype.uuid(),
  avatarUrl: mockImgAvatar(index + 1),
  name: faker.commerce.productName(),
  category: faker.commerce.productName()
}));

export default stores;
