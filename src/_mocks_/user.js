import faker from 'faker';
import { sample } from 'lodash';
// utils
import { mockImgAvatar } from '../utils/mockImages';

// ----------------------------------------------------------------------

const users = [...Array(24)].map((_, index) => ({
  id: faker.datatype.uuid(),
  avatarUrl: mockImgAvatar(index + 1),
  fullname: faker.name.findName(),
  customerOrder: faker.company.companyName(),
  isPayed: faker.datatype.boolean(),
  status: sample(['Доставлен', 'Отменен', 'Ожидает доставки']),
  date: faker.date.past()
}));

export default users;
