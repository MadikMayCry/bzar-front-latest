import { useState } from 'react';
import { Outlet } from 'react-router-dom';
// material
import { Drawer, Stack, Typography, IconButton, Divider } from '@material-ui/core';
import { experimentalStyled as styled } from '@material-ui/core/styles';
import { MHidden } from 'components/@material-extend';
// icons
import { Icon } from '@iconify/react';
import closeFill from '@iconify/icons-eva/close-fill';

// components
import { ProductCartWidget } from 'components/_dashboard/products';
import Scrollbar from 'components/Scrollbar';
import MainNavbar from './MainNavbar';
import CartWidget from './CartWidget';
import BottomNavigation from './BottomNavigation';
// import DashboardSidebar from './DashboardSidebar';

// ----------------------------------------------------------------------

const APP_BAR_MOBILE = 64;
const APP_BAR_DESKTOP = 120;

const RootStyle = styled('div')({
  display: 'flex',
  minHeight: '100%',
  overflow: 'hidden'
});

const MainStyle = styled('div')(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  position: 'relative',
  minHeight: '100%',
  paddingTop: APP_BAR_MOBILE + 24,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('md')]: {
    paddingTop: APP_BAR_DESKTOP + 60,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  }
}));

const ScrollStyle = styled(Scrollbar)(({ theme }) => ({
  minWidth: '100vw',
  [theme.breakpoints.up('sm')]: {
    minWidth: '500px'
  }
}));
// ----------------------------------------------------------------------

export default function DashboardLayout() {
  const [showCart, setShowCart] = useState(false);

  const toggleDrawer = (open) => (event) => {
    console.log(open, event);
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    console.log(open);

    setShowCart(open);
  };

  return (
    <RootStyle>
      <MainNavbar />
      {/* <DashboardSidebar isOpenSidebar={open} onCloseSidebar={() => setOpen(false)} /> */}
      <MainStyle>
        <Outlet />
        <ProductCartWidget toggleDrawer={toggleDrawer} />
        <Drawer
          anchor="right"
          open={showCart}
          onClose={toggleDrawer(false)}
          PaperProps={{
            sx: { border: 'none', overflowY: 'hidden' }
          }}
        >
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            sx={{ px: 1, py: 2 }}
          >
            <Typography variant="subtitle1" sx={{ ml: 1 }}>
              Корзина
            </Typography>
            <IconButton onClick={toggleDrawer(false)}>
              <Icon icon={closeFill} width={20} height={20} />
            </IconButton>
          </Stack>

          <Divider />
          <ScrollStyle>
            <CartWidget close={toggleDrawer(false)} />
          </ScrollStyle>
        </Drawer>
        <MHidden width="mdUp">
          <BottomNavigation />
        </MHidden>
      </MainStyle>
    </RootStyle>
  );
}
