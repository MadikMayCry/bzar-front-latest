import React from 'react';
import PropTypes from 'prop-types';
// material
import { experimentalStyled as styled } from '@material-ui/core/styles';
import { Box, Stack, Typography, IconButton, Divider } from '@material-ui/core';
import { Icon } from '@iconify/react';
import plusFill from '@iconify/icons-eva/plus-fill';
import minusFill from '@iconify/icons-eva/minus-fill';
// redux
import { useDispatch } from 'react-redux';
import { incrementQuantity, decrementQuantity } from 'features/cart/cartSlice';
//
import { fCurrency } from 'utils/formatNumber';
import { isEmptyObject } from 'utils/helpers';

const ProductImg = styled('img')({
  width: '64px'
});

CartItem.propTypes = {
  product: PropTypes.shape({
    cover: PropTypes.string,
    name: PropTypes.string,
    quantity: PropTypes.number,
    price: PropTypes.number,
    index: PropTypes.number,
    type: PropTypes.string,
    sauce: PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.string,
      price: PropTypes.number
    })
  })
};

function CartItem({ product }) {
  const { cover, name, quantity, price, index, type, sauce } = product;
  const dispatch = useDispatch();

  const finalPrice = quantity * price + (isEmptyObject(sauce) ? 0 : sauce.price * quantity);

  const handleIncrement = () => {
    dispatch(incrementQuantity(product));
  };

  const handleDecrement = () => {
    dispatch(decrementQuantity(product));
  };

  return (
    <>
      {index !== 0 && <Divider orientation="horizontal" />}

      <Stack flex direction="column" alignItems="center" sx={{ my: 2 }}>
        <Stack flex direction="row" alignItems="center" sx={{ width: '100%' }}>
          <ProductImg alt={name} src={cover} sx={{ mr: 2 }} />
          <Stack sx={{ overflow: 'hidden' }}>
            <Typography variant="body1" noWrap>
              {name}
            </Typography>
            {type === 'auzt' && (
              <Typography variant="caption" noWrap>
                {sauce.label}
              </Typography>
            )}
          </Stack>
        </Stack>
        <Stack
          flex
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          sx={{ width: '100%', mt: 1 }}
        >
          <Box>
            <IconButton variant="contained" onClick={handleDecrement}>
              <Icon icon={minusFill} width={20} height={20} />
            </IconButton>
            {quantity}
            <IconButton color="inherit" onClick={handleIncrement}>
              <Icon icon={plusFill} width={20} height={20} />
            </IconButton>
          </Box>
          <Box sx={{ ml: 'auto' }}>{fCurrency(finalPrice)} тг</Box>
        </Stack>
      </Stack>
    </>
  );
}

export default CartItem;
