import React, { useState } from 'react';
import { experimentalStyled as styled } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { Button } from '@material-ui/core';
// import db from 'utils/db.json';
import db from 'utils/db';

const Menu = () => (
  <Wrapper>
    <Row className="d-flex">
      {db.categories.map((category) => (
        <MenuItem key={category.name} menu={category} />
      ))}
    </Row>
  </Wrapper>
);

const MenuItem = ({ menu }) => {
  const [hovered, setHovered] = useState(false);
  const toggleLi = () => setHovered((prev) => !prev);
  return (
    <Item
      onMouseEnter={toggleLi}
      onMouseLeave={toggleLi}
      className={`${hovered ? 'sub-cat-show' : ''}`}
    >
      {menu.name}
      <div className="hl-cat-nav__flyout">
        <div className="hl-cat-nav__sub-cats">
          <div className="hl-cat-nav__sub-cat-col">
            <span>Популярные категории</span>
            <ul
              style={{
                listStyle: 'none'
              }}
            >
              {menu.subCategories.map((item) => (
                <li key={item.name}>
                  <Button
                    variant="text"
                    to={`/category/${menu.code}/${item.code}`}
                    component={RouterLink}
                  >
                    {item.name}
                  </Button>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </Item>
  );
};

const Wrapper = styled('div')(({ theme }) => ({
  maxWidth: '100%',
  width: '100%',
  '& li.sub-cat-show .hl-cat-nav__flyout': {
    display: 'block'
  },

  '& .hl-cat-nav__flyout': {
    backgroundColor: '#fff',
    border: 'solid 1px #ddd',
    borderTop: 'solid 1px #fff',
    display: 'none',
    left: 0,
    padding: '10px 10px 10px 0',
    position: 'absolute',
    right: 0,
    textAlign: 'left',
    top: '40px',
    zIndex: 999,
    background: '#fbfbfb',
    boxShadow: '0px 4px 44px rgba(173,173,173,0.25)',

    '& ul': {
      paddingLeft: 0
    }
  },

  '& .hl-cat-nav__sub-cat-col': {
    paddingLeft: '16px',
    paddingRight: '16px'
  },
  '& .hl-cat-nav__flyout span': {
    color: '#333',
    display: 'block',
    fontWeight: 700,
    marginBottom: '13px',
    marginTop: '11px',
    paddingBottom: '7px'
  },

  '& .hl-cat-nav ul': {
    fontSize: '12px',
    marginBottom: 0,
    marginTop: 0,
    paddingLeft: 0,
    whiteSpace: 'normal'
  },

  '& .hl-cat-nav li': {
    borderLeft: 'solid1pxtransparent',
    borderRight: 'solid1pxtransparent',
    webkitBoxSizing: 'border-box',
    boxSizing: 'border-box',
    display: 'inline-block',
    marginBottom: '-1px',
    lineHeight: '16px',
    padding: '9px 12px 9px'
  }
}));

const Row = styled('ul')(({ theme }) => ({
  whiteSpace: 'nowrap',
  position: 'relative',
  padding: '10px0',
  display: 'flex',
  justifyContent: 'space-around',
  '& ::-webkit-scrollbar': {
    height: '3px',
    borderRadius: '20px',
    background: 'white'
  },
  '& ::-webkit-scrollbar-track': {
    boxShadow: 'inset 0 0 2px rgba(0, 0, 0, 0.3)'
  },
  '& ::-webkit-scrollbar-thumb': {
    backgroundColor: 'rgb(145 132 132 / 0.5)'
  }
}));

const Item = styled('li')(({ theme }) => ({
  cursor: 'pointer',
  fontStyle: 'normal',
  fontWeight: 500,
  textAlign: 'center',
  letterSpacing: '0.03em',
  margin: '0 10px',
  padding: '10px 0',
  listStyle: 'none',
  color: theme.palette.text.primary
}));

export default Menu;
