import React from 'react';
import { Icon } from '@iconify/react';
import { experimentalStyled as styled } from '@material-ui/core/styles';
// material
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core';

import roundHome from '@iconify/icons-ic/round-home';
import roundGridView from '@iconify/icons-ic/round-grid-view';
import baselinePerson from '@iconify/icons-ic/baseline-person';

const RootStyle = styled(BottomNavigation)(({ theme }) => ({
  position: 'fixed',
  borderTop: '1px solid #dee2e6 !important',
  bottom: -1,
  left: 0,
  background: 'white',
  width: '100%',
  height: 'auto',
  padding: '5px 0',
  zIndex: 1002
}));

const getIcon = (name) => <Icon icon={name} width={28} height={28} style={{ marginBottom: 5 }} />;

function BottomMenu() {
  const [value, setValue] = React.useState(0);

  return (
    <RootStyle
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
    >
      <BottomNavigationAction label="Главная" icon={getIcon(roundHome)} />
      <BottomNavigationAction label="Категории" icon={getIcon(roundGridView)} />
      <BottomNavigationAction label="Кабинет" icon={getIcon(baselinePerson)} />
    </RootStyle>
  );
}

export default BottomMenu;
