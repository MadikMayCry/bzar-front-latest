import React, { useState } from 'react';
// redux
import { useSelector, useDispatch } from 'react-redux';
import { clearCart } from 'features/cart/cartSlice';
// material
import { Stack, Box, Divider, Typography, Button, TextField } from '@material-ui/core';
import { experimentalStyled as styled } from '@material-ui/core/styles';
// Icons
import { Icon } from '@iconify/react';
import outlineFoodBank from '@iconify/icons-ic/outline-food-bank';
import baselineCreditCard from '@iconify/icons-ic/baseline-credit-card';
import baselineAttachMoney from '@iconify/icons-ic/baseline-attach-money';
import baselineAppRegistration from '@iconify/icons-ic/baseline-app-registration';
import baselineMoped from '@iconify/icons-ic/baseline-moped';
import roundClearAll from '@iconify/icons-ic/round-clear-all';
// import ScrollBar from 'components'

// components
// import { CartButtons, ModalEmployeeLunch, ModalMixedPayment } from 'components/_dashboard/mealMenu';
import { fCurrency } from 'utils/formatNumber';
import { useNavigate } from 'react-router-dom';
import CartItem from './CartItem';

const LUNCH = 'Обед';

const RootStyle = styled(Box)(() => ({
  maxWidth: '100%'
}));

function CardWidget({ close }) {
  // navigate
  const navigate = useNavigate();
  // redux
  const dispatch = useDispatch();
  const { cartList, totalPrice } = useSelector((state) => state.cart);

  const handleClear = () => dispatch(clearCart());

  const handleSubmit = (e) => {
    close(e);
    navigate('/checkout');
  };

  return (
    <RootStyle sx={{ p: 2 }}>
      {cartList.map((cartItem, index) => (
        <CartItem key={index} product={cartItem} index={index} />
      ))}
      <Divider orientation="horizontal" sx={{ my: 2 }} />
      <Stack direction="row" justifyContent="space-between" alignItems="center">
        <Typography variant="button" display="block" gutterBottom>
          Текущая сумма
        </Typography>
        <Typography variant="button" display="block" gutterBottom>
          {fCurrency(totalPrice)} тг
        </Typography>
      </Stack>
      <Divider orientation="horizontal" sx={{ my: 2 }} />
      <Button
        fullWidth
        color="inherit"
        variant="outlined"
        onClick={handleClear}
        startIcon={<Icon icon={roundClearAll} />}
      >
        Очистить
      </Button>
      <Divider orientation="horizontal" sx={{ my: 2 }} />

      <Button variant="contained" fullWidth onClick={handleSubmit} disabled={!cartList[0]}>
        Оформить заказ
      </Button>
      {/*
      
      <Typography variant="body1">Тип оплаты: {paymentType}</Typography>
      <CartButtons
        options={PAYMENT_OPTIONS}
        paymentValue={paymentType}
        handleClick={handlePaymentType}
      />
      <Divider orientation="horizontal" sx={{ my: 2 }} />
      <TextField
        label="Имя покупателя"
        value={customerName}
        onChange={handleCustomerName}
        fullWidth
        sx={{ mb: 2 }}
        size="small"
      />
      <TextField
        label="Промокод"
        value={promocode}
        onChange={handlePromocode}
        fullWidth
        size="small"
      />
*/}
    </RootStyle>
  );
}

export default CardWidget;

const PAYMENT_OPTIONS = [
  {
    name: 'Наличный',
    icon: baselineAttachMoney
  },
  {
    name: 'Картой',
    icon: baselineCreditCard
  },
  {
    name: 'Смешанный',
    icon: baselineAppRegistration
  },
  {
    name: 'Доставка',
    icon: baselineMoped
  },
  {
    name: LUNCH,
    icon: outlineFoodBank
  }
];
