import { Link as RouterLink } from 'react-router-dom';
// material
import { alpha, experimentalStyled as styled } from '@material-ui/core/styles';
import { Container, Stack, AppBar, Toolbar, Divider } from '@material-ui/core';
// components
import Logo from 'components/Logo';
import { MHidden } from 'components/@material-extend';
//
// import Searchbar from './Searchbar';
import AccountPopover from 'components/AccountPopover';
import ThemeSwitcher from 'components/ThemeSwitcher';
import LanguagePopover from 'components/LanguagePopover';
import NotificationsPopover from 'components/NotificationsPopover';
import CategoryMenu from './CategoryMenu';
//
//
import Searchbar from './Searchbar';

// ----------------------------------------------------------------------

const APPBAR_MOBILE = 64;
const APPBAR_DESKTOP = 140;

const RootStyle = styled(AppBar)(({ theme }) => ({
  boxShadow: 'none',
  backdropFilter: 'blur(6px)',
  WebkitBackdropFilter: 'blur(6px)', // Fix on Mobile
  backgroundColor: alpha(theme.palette.background.default, 0.72),
  padding: 0
  // background: 'radial-gradient(103.57% 5048.49% at 0% 0%, #F65E69 0%, #FFAD64 45.23%, #A804AB 100%)'
}));

const ToolbarStyle = styled(Toolbar)(({ theme }) => ({
  minHeight: APPBAR_MOBILE,
  flexWrap: 'wrap',
  [theme.breakpoints.up('lg')]: {
    minHeight: APPBAR_DESKTOP,
    padding: theme.spacing(2.5, 0)
  }
}));
// ----------------------------------------------------------------------

export default function DashboardNavbar() {
  return (
    <RootStyle>
      <Container>
        <ToolbarStyle>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            sx={{ flexGrow: 1, flex: '0 0 100%' }}
          >
            <RouterLink to="/">
              <Logo />
            </RouterLink>
            <Searchbar />

            <Stack direction="row" alignItems="center" spacing={{ xs: 0.5, sm: 1.5 }} ml="auto">
              <LanguagePopover />
              <NotificationsPopover />
              <ThemeSwitcher />
              <AccountPopover />
            </Stack>
          </Stack>
          <MHidden width="mdDown">
            <Divider orientation="horizontal" sx={{ mt: 2, mb: 1, width: '100%' }} />
            <CategoryMenu />
          </MHidden>
        </ToolbarStyle>
      </Container>
    </RootStyle>
  );
}
