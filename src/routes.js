import { Navigate, useRoutes } from 'react-router-dom';

//
import LoginComplete from 'pages/LoginComplete';
import Login from 'pages/Login';
import Register from 'pages/Register';
import DashboardApp from 'pages/DashboardApp';
import Products from 'pages/Products';
import Blog from 'pages/Blog';
import CustomerOrders from 'pages/CustomerOrders';
import NotFound from 'pages/Page404';
import Catalog from 'pages/Catalog';
import Main from 'pages/Main';
import Cart from 'pages/Cart';
import CheckOut from 'pages/Checkout';
import Delivery from 'pages/Delivery';
import LogoutPage from 'pages/Logout';
import ProductDetails from 'pages/ProductDetails';
import Store from 'pages/Store';
import Messenger from 'pages/Messenger';

// layouts
import DashboardLayout from 'layouts/dashboard';
import MainLayout from 'layouts/main';
import LogoOnlyLayout from './layouts/LogoOnlyLayout';

// ----------------------------------------------------------------------

export default function Router() {
  return useRoutes([
    {
      path: '/dashboard',
      element: <DashboardLayout />,
      children: [
        { path: '/', element: <Navigate to="/dashboard/app" replace /> },
        { path: 'app', element: <DashboardApp /> },
        { path: 'customer-orders', element: <CustomerOrders /> },
        { path: 'products', element: <Products /> },
        { path: 'blog', element: <Blog /> },
        { path: 'delivery', element: <Delivery /> }
      ]
    },
    {
      path: '/auth',
      element: <LogoOnlyLayout />,
      children: [
        { path: 'login', element: <Login /> },
        { path: 'register', element: <Register /> }
        // { path: '404', element: <NotFound /> }
        // { path: '*', element: <Navigate to="/404" /> }
      ]
    },
    {
      path: '/',
      element: <MainLayout />,
      children: [
        { path: '/', element: <Main /> },
        // { path: 'catalog', element: <Catalog /> },
        { path: 'store', element: <Store /> },
        { path: 'messenger', element: <Messenger /> },
        { path: 'category/:catId/:id', element: <Catalog /> },
        { path: 'product-details/:productId', element: <ProductDetails /> },
        { path: 'cart', element: <Cart /> },
        { path: 'login_complete', element: <LoginComplete /> },
        { path: 'checkOut', element: <CheckOut /> },
        { path: 'logout', element: <LogoutPage /> },
        { path: '404', element: <NotFound /> }
      ]
    },
    { path: '*', element: <Navigate to="/404" replace /> }
  ]);
}
